﻿using System;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day3BinaryDiagnostic
    {
        public static int Part1(string input, int bitCount)
        {
            var gamma = 0;
            var epsilon = 0;

            var bitSum = new int[bitCount];
            var lineCount = 0;

            foreach (var line in input.ReadLines())
            {
                Enumerable.Range(0, bitCount)
                    .Where(i => line[i] == '1')
                    .ForEach(i => bitSum[i]++);
                lineCount++;
            }

            var average = lineCount / 2;
            foreach (var sum in bitSum)
            {
                gamma <<= 1;
                epsilon <<= 1;

                if (sum >= average)
                {
                    gamma++;
                }
                else
                {
                    epsilon++;
                }
            }

            return gamma * epsilon;
        }

        class Node
        {
            public int Count { get; set; }
            public int Bit { get; set; }
            public Node[] Nodes { get; set; }

            public Node(int bit)
            {
                Bit = bit;
            }

            public int Search(Func<int, int, bool> compare)
            {
                var selectFirstNode = Nodes[0].Count + Nodes[1].Count == 1
                    ? Nodes[0].Count == 1
                    : compare(Nodes[0].Count, Nodes[1].Count);
                var matchIndex = selectFirstNode ? 0 : 1;
                var match = Nodes[matchIndex];
                if (Bit == 0 || match.Nodes == null) 
                    return matchIndex;
                var result = match.Search(compare);
                return (matchIndex << match.Bit) + result;
            }
        }

        public static int Part2(string input, int bitCount)
        {
            var root = new Node(bitCount);

            foreach (var line in input.ReadLines())
            {
                var node = root;
                foreach (var index in Enumerable.Range(0, bitCount).Select(i => line[i] == '1' ? 1 : 0))
                {
                    node.Nodes ??= new Node[] { new Node(node.Bit - 1), new Node(node.Bit - 1) };
                    node = node.Nodes[index];
                    node.Count++;
                }
            }

            var gamma = root.Search((left, right) => left > right);
            var epsilon = root.Search((left, right) => left <= right);

            return gamma * epsilon;
        }
    }
}
