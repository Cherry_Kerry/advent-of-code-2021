﻿using System.Collections.Generic;
using System.IO;

namespace AdventOfCode
{
    public static class Day1SonarSweep
    {
        public static int Part1(string input)
        {
            var result = 0;

            using (var reader = new StringReader(input))
            {
                var line = reader.ReadLine();
                var previous = int.Parse(line);
                while ((line = reader.ReadLine()) != null)
                {
                    var current = int.Parse(line);

                    if (current > previous)
                        result++;

                    previous = current;
                }
            }

            return result;
        }

        public static int Part2(string input)
        {
            var result = 0;
            var windowSize = 3;

            using (var reader = new StringReader(input))
            {
                var line = reader.ReadLine();
                var values = new Queue<int>(windowSize);
                var previousSum = int.Parse(line);
                values.Enqueue(previousSum);
                while ((line = reader.ReadLine()) != null)
                {
                    var current = int.Parse(line);
                    values.Enqueue(current);

                    var currentSum = previousSum + current;

                    if (values.Count > windowSize)
                        currentSum -= values.Dequeue();
                    else
                        continue;

                    if (currentSum > previousSum)
                        result++;

                    previousSum = currentSum;
                }
            }

            return result;
        }
    }
}
