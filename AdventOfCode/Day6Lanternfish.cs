﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;

namespace AdventOfCode
{
    public static class Day6Lanternfish
    {
        public static BigInteger[] ChildrenRefference(int days)
        {
            var children = new BigInteger[days + 8];
            var age = 9;
            BigInteger childrenCount = 0;

            foreach (var index in Enumerable.Range(0, children.Length))
            {
                children[index] = childrenCount;
                age--;
                if (age == 0)
                {
                    childrenCount++;
                    age = 7;
                }
            }

            return children;
        }

        public static BigInteger[] Children(int days)
        {
            var childrenRefference = ChildrenRefference(days);
            var children = childrenRefference.Select(x => x +1).ToArray();
            BigInteger childrenCount = 1;

            foreach (var index in Enumerable.Range(0, children.Length))
            {
                var childDiff = children[index] - childrenCount;
                if (childDiff > 0)
                {
                    foreach (var childIndex in Enumerable.Range(0, children.Length - index))
                    {
                        children[index+childIndex] += childrenRefference[childIndex] * childDiff;
                    }
                    childrenCount = children[index];
                }
            }

            return children;
        }

        public static string Run(string input, int days)
        {
            var children = Children(days);
            BigInteger count = 0;

            var fishAges = input.ReadLines().First().Split(',').Select(n => int.Parse(n));

            foreach (var age in fishAges)
            {
                var index = days + (8 - age);
                count += children[index];
            }

            return count.ToString();
        }
    }
}
