﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    public static class Day9SmokeBasin
    {
        public static int Part1(string input, int rows, int cols)
        {
            var points = new int[rows, cols];

            foreach (var (line, row) in input.ReadLines().Select((line,index) => (line,index)))
            {
                foreach(var (point, col) in line.Select((point, index) => (point, index)))
                {
                    points[row, col] = int.Parse(point.ToString());
                }
            }

            bool IsLowPoint(int row, int col)
            {
                var value = points[row, col];
                if (value == 0)
                    return true;

                var check = new List<int>
                {
                    value
                };

                if (row > 0)
                    check.Add(points[row-1, col]);
                if (col > 0)
                    check.Add(points[row, col-1]);
                if (row+1 < rows)
                    check.Add(points[row+1, col]);
                if (col+1 < cols)
                    check.Add(points[row, col+1]);

                return value == check.Min() && check.Count(x => x == value) == 1;

            }

            var result = 0;

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    if (IsLowPoint(row, col))
                    {
                        result += points[row, col] + 1;
                    }
                }
            }

            return result;
        }

       

        public static int Part2(string input, int rows, int cols)
        {
            var points = new int[rows, cols];

            foreach (var (line, row) in input.ReadLines().Select((line, index) => (line, index)))
            {
                foreach (var (point, col) in line.Select((point, index) => (point, index)))
                {
                    points[row, col] = int.Parse(point.ToString());
                }
            }

            bool IsLowPoint(int row, int col)
            {
                var value = points[row, col];
                if (value == 0)
                    return true;

                var check = new List<int>
                {
                    value
                };

                if (row > 0)
                    check.Add(points[row - 1, col]);
                if (col > 0)
                    check.Add(points[row, col - 1]);
                if (row + 1 < rows)
                    check.Add(points[row + 1, col]);
                if (col + 1 < cols)
                    check.Add(points[row, col + 1]);

                return value == check.Min() && check.Count(x => x == value) == 1;
            }

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    if (IsLowPoint(row, col))
                    {
                        var point = points[row, col];
                        var basin = new HashSet<(int row, int col)>
                        {
                            (row, col)
                        };
                        foreach (var item in Enumerable.Range(point+1, 9))
                        {
                            var startCount = basin.Count();

                            

                            if (startCount == basin.Count())
                                break;
                        }
                    }
                }
            }


        }
    }
}
