﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day4GiantSquid
    {
        class Board
        {
            public int Index { get; set; }
            public Board(int index)
            {
                Index = index;
            }
            public Number[,] Numbers { get; } = new Number[5,5];

            internal bool HasBingo(Number number)
            {
                return Enumerable.Range(0, 5).Select(i => Numbers[number.Row, i].Marked).All(x => x)
                    || Enumerable.Range(0, 5).Select(i => Numbers[i, number.Column].Marked).All(x => x);
            }

            internal int SumUnmarked()
            {
                var sum = 0;
                foreach (int row in Enumerable.Range(0, 5))
                {
                    foreach (int col in Enumerable.Range(0, 5))
                    {
                        if (!Numbers[row, col].Marked)
                            sum += Numbers[row, col].Value;
                    }
                }
                return sum;
            }
        }
    
        class Number
        {
            public Board Board { get; set; }
            public int Row { get; set; }
            public int Column { get; set; }
            public bool Marked { get; set; }
            public int Value { get; set; }

            public override string ToString()
            {
                return new { Value, Marked}.ToString();
            }
        }

        public static int Part1(string input)
        {
            var split = input.Split("\r\n\r\n");
            var numbers = split[0].Split(",").Select(n => int.Parse(n));
            var boards = split.Skip(1);

            var numberArray = new List<Number>[100];

            foreach (var (board, boardData) in boards.Select(boardData => (new Board(0), boardData)))
                foreach (var (row, rowIndex) in boardData.ReadLines().Select((row, rowIndex) => (row, rowIndex)))
                    foreach (var (value, colIndex) in row.Split().Where(r => !string.IsNullOrEmpty(r)).Select((value, colIndex) => (int.Parse(value), colIndex)))
                    {
                        var number = new Number
                        {
                            Board = board,
                            Column = colIndex,
                            Row = rowIndex,
                            Value = value
                        };
                        board.Numbers[rowIndex,colIndex] = number;
                        numberArray[value] ??= new List<Number>();
                        numberArray[value].Add(number);
                    }

            foreach (var number in numbers.Select(n => numberArray[n]).Where(n => numberArray != null).SelectMany(n => n))
            {
                number.Marked = true;
                if (number.Board.HasBingo(number))
                {
                    return number.Board.SumUnmarked() * number.Value;
                }
            }

            return 0;
        }

        public static int Part2(string input)
        {
            var split = input.Split("\r\n\r\n");
            var numbers = split[0].Split(",").Select(n => int.Parse(n));
            var boards = split.Skip(1);

            var numberArray = new List<Number>[100];

            var allBoards = boards.Select((boardData, index) => (index, new Board(index), boardData)).ToDictionary(x => x.index);

            foreach (var (index, board, boardData) in allBoards.Values)
                foreach (var (row, rowIndex) in boardData.ReadLines().Select((row, rowIndex) => (row, rowIndex)))
                    foreach (var (value, colIndex) in row.Split().Where(r => !string.IsNullOrEmpty(r)).Select((value, colIndex) => (int.Parse(value), colIndex)))
                    {
                        var number = new Number
                        {
                            Board = board,
                            Column = colIndex,
                            Row = rowIndex,
                            Value = value
                        };
                        board.Numbers[rowIndex, colIndex] = number;
                        numberArray[value] ??= new List<Number>();
                        numberArray[value].Add(number);
                    }

            foreach (var number in numbers.Select(n => numberArray[n]).Where(n => numberArray != null).SelectMany(n => n).Where(n => allBoards.ContainsKey(n.Board.Index)))
            {
                number.Marked = true;
                if (number.Board.HasBingo(number))
                {
                    if (allBoards.Count == 1)
                        return number.Board.SumUnmarked() * number.Value;
                    allBoards.Remove(number.Board.Index);
                }
            }

            return 0;
        }
    }
}
