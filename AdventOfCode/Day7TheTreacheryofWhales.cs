﻿using System;
using System.Linq;

namespace AdventOfCode
{
    public static class Day7TheTreacheryofWhales
    {
        public static int Part1(string input)
        {
            var values = input.ReadLines()
                .First()
                .Split(',')
                .Select(x => int.Parse(x))
                .ToArray();
            Array.Sort(values);

            var half = values.Length / 2;
            var position = values[half];

            var result = values.Sum(x => Math.Abs(x - position));
            return result;
        }

        public static int Part2(string input)
        {
            var values = input.ReadLines()
                .First()
                .Split(',')
                .Select(x => int.Parse(x))
                .ToArray();
            Array.Sort(values);

            var position = (int)Math.Round(values.Average());
            var result = Enumerable.Range(-1, 3)
                .Select(y => values
                    .Select(x => Math.Abs(x - position + y))
                    .Sum(x => x * (x+1) / 2))
                .Min();

            return result;
        }
    }
}
