﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode
{
    public static class LinqExtension
    {
        public static IEnumerable<string> ReadLines(this string text)
        {
            var reader = new StringReader(text);
            for (var line = reader.ReadLine(); line != null; line = reader.ReadLine())
            {
                yield return line;
            }
        }

        public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
        {
            foreach (var item in source)
            {
                action(item);
            }
        }
    }
}
