﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day5HydrothermalVenture
    {
        public static int Delta(int left, int right)
        {
            var diff = right - left;
            if (diff < 0)
                return -1;
            if (diff > 0)
                return 1;
            return 0;
        }

        public static int Part1(string input, int max)
        {
            var matrix = new int[max, max];
            var hits = 0;

            foreach (var line in input.ReadLines())
            {
                var points = line.Split(" -> ")
                    .Select(point => 
                    {
                        var split = point.Split(",");
                        var x = int.Parse(split[0]);
                        var y = int.Parse(split[1]);
                        return new { x, y };
                    })
                    .ToArray();
                var isLineConsidered = points[0].x == points[1].x || points[0].y == points[1].y;
                if (!isLineConsidered)
                    continue;

                var xStart = points[0].x;
                var xDelta = Delta(points[0].x, points[1].x);
                var xFinish = points[1].x + xDelta;

                var yStart = points[0].y;
                var yDelta = Delta(points[0].y, points[1].y);
                var yFinish = points[1].y + yDelta;

                for (
                    var (x,y) = (xStart, yStart); 
                    !(x == xFinish && y == yFinish) ; 
                    (x,y) = (x+xDelta,y+yDelta))
                {
                    var hitOnce = matrix[x, y] == 1;
                    if (hitOnce)
                        hits++;
                    matrix[x, y]++;
                }
            }

            return hits;
        }

        public static int Part2(string input, int max)
        {
            var matrix = new int[max, max];
            var hits = 0;

            foreach (var line in input.ReadLines())
            {
                var points = line.Split(" -> ")
                    .Select(point =>
                    {
                        var split = point.Split(",");
                        var x = int.Parse(split[0]);
                        var y = int.Parse(split[1]);
                        return new { x, y };
                    })
                    .ToArray();

                var xStart = points[0].x;
                var xDelta = Delta(points[0].x, points[1].x);
                var xFinish = points[1].x + xDelta;

                var yStart = points[0].y;
                var yDelta = Delta(points[0].y, points[1].y);
                var yFinish = points[1].y + yDelta;

                for (
                    var (x, y) = (xStart, yStart);
                    !(x == xFinish && y == yFinish);
                    (x, y) = (x + xDelta, y + yDelta))
                {
                    var hitOnce = matrix[x, y] == 1;
                    if (hitOnce)
                        hits++;
                    matrix[x, y]++;
                }
            }

            return hits;
        }
    }
}
