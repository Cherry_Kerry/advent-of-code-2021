﻿using System.IO;

namespace AdventOfCode
{
    public static class Day2Dive
    {
        public static int Part1(string input)
        {
            var horizontal = 0;
            var depth = 0;

            using (var reader = new StringReader(input))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                { 
                    var lineParts = line.Split(' ');

                    var direction = lineParts[0][0];
                    var value = int.Parse(lineParts[1]);

                    switch (direction)
                    {
                        case 'd':
                            depth += value;
                            break;
                        case 'u':
                            depth -= value;
                            break;
                        default:
                            horizontal += value;
                            break;
                    }
                }
            }

            return horizontal * depth;
        }

        public static int Part2(string input)
        {
            var horizontal = 0;
            var depth = 0;
            var aim = 0;

            using (var reader = new StringReader(input))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var lineParts = line.Split(' ');

                    var direction = lineParts[0][0];
                    var value = int.Parse(lineParts[1]);

                    switch (direction)
                    {
                        case 'd':
                            aim += value;
                            break;
                        case 'u':
                            aim -= value;
                            break;
                        default:
                            horizontal += value;
                            depth += aim * value;
                            break;
                    }
                }
            }

            return horizontal * depth;
        }
    }
}
