﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    public static class Day8SevenSegmentSearch
    {
        public static int Part1(string input)
        {
            var lengths = new[] { 2, 3, 4, 7 };
            var result = input.ReadLines()
                .SelectMany(line => line.Split(" | ")[1].Split(' '))
                .Where(digit => lengths.Contains(digit.Length));
            return result.Count();
        }

        public static int Part2(string input)
        {
            var lengths = new[] { 2, 3, 4, 7 };
            var result = 0;
            foreach (var line in input.ReadLines().Select(line => line.Split(" | ")))
            {
                var inputs = line[0].Split(' ').Select(s => string.Join("", s.OrderBy(c => c)));
                var ouputs = line[1].Split(' ').Select(s => string.Join("", s.OrderBy(c => c)));

                var numbers = new Dictionary<string, char>(); ;

                var one = inputs.Where(x => x.Length == 2).First();
                var four = inputs.Where(x => x.Length == 4).First();
                var seven = inputs.Where(x => x.Length == 3).First();
                var eight = inputs.Where(x => x.Length == 7).First();

                var chars17 = one.Concat(seven).ToHashSet();
                var chars47 = four.Concat(seven).ToHashSet();
                var char6 = inputs.Where(x => x.Length == 6).ToList();
                var char5 = inputs.Where(x => x.Length == 5).ToList();

                var nine = char6.Where(x => chars47.All(c => x.Contains(c))).First();
                char6.Remove(nine);

                var zero = char6.Where(x => chars17.All(c => x.Contains(c))).First();
                char6.Remove(zero);

                var six = char6.First();

                var three = char5.Where(x => chars17.All(c => x.Contains(c))).First();
                char5.Remove(three);

                var five = char5.Where(x => four.Count(c => x.Contains(c)) == 3).First();
                char5.Remove(five);

                var two = char5.First();


                numbers[zero]  = '0';
                numbers[one]   = '1';
                numbers[two]   = '2';
                numbers[three] = '3';
                numbers[four]  = '4';
                numbers[five]  = '5';
                numbers[six]   = '6';
                numbers[seven] = '7'; 
                numbers[eight] = '8';
                numbers[nine]  = '9';

                var output = string.Join("",ouputs.Select(number => numbers[number]));
                result += int.Parse(output);
            }
            return result;
        }

        // ln2 = 1
        // ln3 = 7
        // ln4 = 4
        // ln5 = 2,3,5
        // ln6 = 0,6,9
        // ln7 = 8

        // 0 has 1,7 no 4
        // 6 not 1,7,4
        // 9 has 1,7,4

        // 2 has 1,7 missing f
        // 3 has a 1,7
        // 5 has 1,7,4 missing c

        // 5 has a 4 less c
        // 6 has a 4 less c


    }
}
