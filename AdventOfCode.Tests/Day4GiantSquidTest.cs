using Assent;
using Newtonsoft.Json;
using System.IO;
using Xunit;

namespace AdventOfCode.Tests
{
    public class Day4GiantSquidTest
    {
        [Fact]
        public void ExampleData()
        {
            var input = File.OpenText("Inputs/Day4GiantSquid-Example.txt").ReadToEnd();

            var part1 = Day4GiantSquid.Part1(input);
            var part2 = Day4GiantSquid.Part2(input);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }

        [Fact]
        public void InputsData()
        {
            var input = File.OpenText("Inputs/Day4GiantSquid-Inputs.txt").ReadToEnd();

            var part1 = Day4GiantSquid.Part1(input);
            var part2 = Day4GiantSquid.Part2(input);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }
    }
}
