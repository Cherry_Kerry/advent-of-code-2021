using Assent;
using Newtonsoft.Json;
using System.IO;
using Xunit;

namespace AdventOfCode.Tests
{
    public class Day5HydrothermalVentureTest
    {
        [Fact]
        public void ExampleData()
        {
            var input = File.OpenText("Inputs/Day5HydrothermalVenture-Example.txt").ReadToEnd();

            var part1 = Day5HydrothermalVenture.Part1(input, 10);
            var part2 = Day5HydrothermalVenture.Part2(input, 10);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }

        [Fact]
        public void InputsData()
        {
            var input = File.OpenText("Inputs/Day5HydrothermalVenture-Inputs.txt").ReadToEnd();

            var part1 = Day5HydrothermalVenture.Part1(input, 1000);
            var part2 = Day5HydrothermalVenture.Part2(input, 1000);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }
    }
}
