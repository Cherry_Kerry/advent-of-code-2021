using Assent;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using Xunit;

namespace AdventOfCode.Tests
{
    public class Day6LanternfishTest
    {
        [Fact]
        public void ExampleData()
        {
            var input = File.OpenText("Inputs/Day6Lanternfish-Example.txt").ReadToEnd();

            var part1 = Day6Lanternfish.Run(input, 80);
            var part2 = Day6Lanternfish.Run(input, 256);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }

        [Fact]
        public void Children()
        {
            var part = Day6Lanternfish.Children(256).Select(x => x.ToString());
            var json = JsonConvert.SerializeObject(part, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }

        [Fact]
        public void InputsData()
        {
            var input = File.OpenText("Inputs/Day6Lanternfish-Inputs.txt").ReadToEnd();

            var part1 = Day6Lanternfish.Run(input, 80);
            var part2 = Day6Lanternfish.Run(input, 256);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }
    }
}
