using Assent;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using Xunit;

namespace AdventOfCode.Tests
{
    public class Day9SmokeBasinTest
    {
        [Fact]
        public void ExampleData()
        {
            var input = File.OpenText("Inputs/Day9SmokeBasin-Example.txt").ReadToEnd();

            var part1 = Day9SmokeBasin.Part1(input,5,10);
            var part2 = Day9SmokeBasin.Part2(input,5,10);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }

        [Fact]
        public void InputsData()
        {
            var input = File.OpenText("Inputs/Day9SmokeBasin-Inputs.txt").ReadToEnd();

            var part1 = Day9SmokeBasin.Part1(input,100,100);
            var part2 = Day9SmokeBasin.Part2(input,100,100);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }
    }
}
