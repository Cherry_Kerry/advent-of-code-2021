using Assent;
using Newtonsoft.Json;
using System.IO;
using Xunit;

namespace AdventOfCode.Tests
{
    public class Day3BinaryDiagnosticTest
    {
        [Fact]
        public void ExampleData()
        {
            var input = File.OpenText("Inputs/Day3BinaryDiagnostic-Example.txt").ReadToEnd();

            var part1 = Day3BinaryDiagnostic.Part1(input, 5);
            var part2 = Day3BinaryDiagnostic.Part2(input, 5);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }

        [Fact]
        public void InputsData()
        {
            var input = File.OpenText("Inputs/Day3BinaryDiagnostic-Inputs.txt").ReadToEnd();

            var part1 = Day3BinaryDiagnostic.Part1(input, 12);
            var part2 = Day3BinaryDiagnostic.Part2(input, 12);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }
    }
}
