using Assent;
using Newtonsoft.Json;
using System.IO;
using Xunit;

namespace AdventOfCode.Tests
{
    public class Day1SonarSweepTest
    {
        [Fact]
        public void ExampleData()
        {
            var input = File.OpenText("Inputs/Day1SonarSweep-Example.txt").ReadToEnd();

            var part1 = Day1SonarSweep.Part1(input);
            var part2 = Day1SonarSweep.Part2(input);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }

        [Fact]
        public void InputsData()
        {
            var input = File.OpenText("Inputs/Day1SonarSweep-Inputs.txt").ReadToEnd();

            var part1 = Day1SonarSweep.Part1(input);
            var part2 = Day1SonarSweep.Part2(input);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }
    }
}
