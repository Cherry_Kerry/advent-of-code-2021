using Assent;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using Xunit;

namespace AdventOfCode.Tests
{
    public class Day7TheTreacheryofWhalesTest
    {
        [Fact]
        public void ExampleData()
        {
            var input = File.OpenText("Inputs/Day7TheTreacheryofWhales-Example.txt").ReadToEnd();

            var part1 = Day7TheTreacheryofWhales.Part1(input);
            var part2 = Day7TheTreacheryofWhales.Part2(input);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }

        [Fact]
        public void InputsData()
        {
            var input = File.OpenText("Inputs/Day7TheTreacheryofWhales-Inputs.txt").ReadToEnd();

            var part1 = Day7TheTreacheryofWhales.Part1(input);
            var part2 = Day7TheTreacheryofWhales.Part2(input);

            var test = new
            {
                part1,
                part2
            };
            var json = JsonConvert.SerializeObject(test, Defaults.JsonSerializerSettings);
            this.Assent(json, Defaults.AssentConfiguration);
        }
    }
}
