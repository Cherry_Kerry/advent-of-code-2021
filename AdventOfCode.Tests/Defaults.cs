﻿using Assent;
using Newtonsoft.Json;

namespace AdventOfCode.Tests
{
    public class Defaults
    {
        public static readonly Configuration AssentConfiguration = new Configuration().UsingExtension("json");
        public static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
        };
    }
}
